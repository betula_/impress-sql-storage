var
  should = require('should'),
  Storage = require('..')
  ;

describe('impress-sql-storage', function () {

  describe('sqlite', function() {

    it('should work', function(done) {
      var
        storage = new Storage(),
        obj = {
          url: 'http://test',
          content: 'text-1'
        },
        objChanged = {
          url: 'http://test',
          content: 'text-2'
        };

      storage.get(obj.url, function(err, data) {
        if (err) return done(err);

        should(data).empty;

        storage.put(obj, function(err) {
          if (err) return done(err);

          storage.get(obj.url, function(err, result) {
            if (err) return done(err);

            should(result.url).equal(obj.url);
            should(result.content).equal(obj.content);

            storage.put(objChanged, function(err) {
              if (err) return done(err);

              storage.get(objChanged.url, function(err, result) {
                if (err) return done(err);

                should(result.url).equal(objChanged.url);
                should(result.content).equal(objChanged.content);

                storage._driver._ImpressModel.count().then(function(count) {
                  should(count).equal(1);

                  storage._driver._ImpressModel.drop().then(function() {
                    done();
                  }, done);

                }, done);

              });
            });
          })
        });

      });

    });

    it('should work with big content', function(done) {
      var
        storage = new Storage(),
        content,
        index,
        obj = {
          url: 'http://test'
        };

      for (index = 0; index < 1024 * 1024; index++) {
        content += 'Ω≈ç∂ƒ';
      }
      obj.content = content;

      storage.put(obj, function(err) {
        if (err) return done(err);

        storage.get(obj.url, function(err, result) {
          if (err) return done(err);

          should(result.url).equal(obj.url);
          should(result.content).equal(obj.content);

          done();
        });
      });
    });

  });

});
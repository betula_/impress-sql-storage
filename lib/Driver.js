var
  ImpressModelFactory = require('./models/Impress'),
  Sequelize = require('sequelize'),
  Promise = require('bluebird');

module.exports = Driver;

function Driver(options) {
  this.options = options;

  this._initReadyPromise();
  this._initSequelize();
  this._initModels();
}


Driver.prototype = {

  _initReadyPromise: function() {
    var
      self = this;
    this._readyPromise = new Promise(function(resolve, reject) {
      self._readyResolve = resolve;
      self._readyReject = reject;
    });
  },

  _initSequelize: function() {
    var
      uri = null,
      database,
      username,
      password,
      options
      ;

    options = this.options;

    if (typeof options == 'string') {
      uri = options;
      options = {};
    }
    else if (options && typeof options == 'object') {
      database = options.database;
      username = options.username;
      password = options.password;
    }
    else {
      uri = 'sqlite:';
      options = {};
    }

    if (typeof options.logging == 'undefined') {
      options.logging = false;
    }

    if (uri) {
      this._sequelize = new Sequelize(uri, options);
    }
    else {
      this._sequelize = new Sequelize(database, username, password, options);
    }
  },

  _initModels: function() {
    this._ImpressModel = ImpressModelFactory(this._sequelize);
    this._sequelize
      .sync()
      .then(this._readyResolve, this._readyReject);
  },

  get: function(url) {
    var
      Model = this._ImpressModel,
      promise;

    promise = this._readyPromise
      .then(function() {
        return Model.find({ url: url }).then(function(model) {
          return model
            ? model.get({ plain: true })
            : null;
        });
      });

    return promise;
  },

  put: function(data) {
    var
      Model = this._ImpressModel,
      promise;

    promise = this._readyPromise
      .then(function() {
        return Model.upsert(data);
      });

    return promise;
  }

};
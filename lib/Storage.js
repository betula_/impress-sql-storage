var
  Driver = require('./Driver');

module.exports = Storage;

function Storage(options) {
  this.options = options;
  this._init();
}

Storage.prototype = {

  _init: function() {
    this._driver = new Driver(this.options);
  },

  get: function(key, callback) {
    var
      promise;
    promise = this._driver.get(key);
    promise.then(
      function(data) {
        callback && callback(null, data);
      },
      function(error) {
        callback && callback(error);
      }
    );
    return promise;
  },

  put: function(value, callback) {
    var
      promise;
    promise = this._driver.put(value);
    promise.then(
      function() {
        callback && callback();
      },
      function(error) {
        callback && callback(error);
      }
    );
    return promise;
  }

};